FROM ubuntu:18.04

RUN apt-get update && apt-get install -y python3-pip

RUN apt-get install -y apt-utils
RUN apt-get install -y openjdk-11-jdk
RUN apt-get install -y scala
RUN apt-get install -y git
RUN apt-get install -y tofrodos
RUN apt-get install -y python3-virtualenv

RUN mkdir /scling_exercise
WORKDIR /scling_exercise

RUN python3 -m virtualenv --python=python3 /scling_exercise_venv
COPY workflow/requirements.txt /
RUN bash -c "source /scling_exercise_venv/bin/activate && pip install -r /requirements.txt"
COPY venv_activate.sh /
ENTRYPOINT ["/venv_activate.sh"]
