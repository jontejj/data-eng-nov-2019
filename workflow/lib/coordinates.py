# You can tweak the contents in this file in order to get things working of necessary.
from functools import lru_cache
from pathlib import Path


PROD_LAKE = "/shared/nfs/lake/dir"  # Not used

TEST_LAKE = "/var/tmp/tiny_big_data/workflow"

LAKE = TEST_LAKE
# In production, if we were using a lake in a shared NFS directory, we could point our workflows there.
# LAKE = PROD_LAKE

LUIGI = 'luigi'
# If false, run luigi as a separate process. If true, call luigi as a library. The former is better isolated,
# the latter is easier to debug
INTERNAL_LUIGI = True

# If true, start MySQL in Docker from the test harness. If false, a MySQL instance must already be running,
# listening on 127.0.0.1.
MYSQL_DOCKER = True
# Used of MYSQL_DOCKER is false.
MYSQL_HOST = '127.0.0.1'
# What docker uses on Ubuntu?
# MYSQL_HOST = '172.17.0.2'


@lru_cache()
def find_src_root(candidate=None):
    # Assume that current directory is somewhere in the source tree. Find the source root.
    if candidate is None:
        return find_src_root(Path.cwd())
    if candidate == Path('/'):
        raise RuntimeError("Must run inside a source tree")
    if (candidate / 'gradlew').exists():
        return candidate
    return find_src_root(candidate.parent)


PYTHONPATH = [str(find_src_root() / p) for p in ['workflow']]
PATH = [str(find_src_root() / p) for p in ['workflow/bin']]
