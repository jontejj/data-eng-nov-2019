import logging
from contextlib import closing
from datetime import datetime, time
from pathlib import Path

import mysql.connector
from luigi import DateParameter, Parameter, Task, OptionalParameter
from luigi import ExternalTask

from lib.coordinates import LAKE
from lib.dataset import write_json_lines
from lib.local_target import LocalFlagTarget


class UserDbSnapshot(Task):
    """Dump a snapshot of the user database table to a lake dataset. The database owners state that
    you are only allowed to dump the database after 1 am, when the production load typically has declined.

    The test harness will set up MySQL in a container for you if you have docker installed on your
    machine. This test requires docker. It has not been tested on windows, and may very well fail
    due to line termination issues. You may try to change coordinates.MYSQL_DOCKER and start MySQL manually.
    """

    date = DateParameter()

    # significant=False tells Luigi to ignore this parameter in the unique id of the task. It is used when the
    # parameter value does not affect the output. It is debatable whether to use it for the host parameter.
    host = Parameter(significant=False)
    # Pass current time for testing purposes.
    now = OptionalParameter(significant=False, default='')

    # Insert solution code here. Most Luigi tasks need one or more of:
    # job parameters (date, etc), requires() method, and output() or complete() method,
    # as well as methods specific to type of task, e.g. program_args (ExternalProgramTask), database (CopyToTable), ...


