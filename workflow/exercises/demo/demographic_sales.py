from luigi import DateParameter
from luigi import ExternalTask
from luigi.contrib.external_program import ExternalProgramTask

from lib.coordinates import LAKE
from lib.local_target import LocalFlagTarget


class DemographicSales(ExternalProgramTask):
    """Read sales and user data each day. Join them with sales_user_join.py."""

    # Insert solution code here. Most Luigi tasks need one or more of:
    # job parameters (date, etc), requires method, output or complete method,
    # as well as methods specific to type of task: program_args (ExternalProgramTask), app_options (SparkSubmitTask)


