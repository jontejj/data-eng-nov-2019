# noinspection PyUnresolvedReferences
import datetime
from pathlib import Path
# noinspection PyUnresolvedReferences
from unittest import TestCase, skip

from lib.coordinates import TEST_LAKE
from test.harness import run_luigi, read_dataset, DbWorkflowTestcase


class DbDumpTest(DbWorkflowTestcase):
    def setUp(self):
        super().setUp()
        self.db_execute('drop table if exists user')
        self.db_execute('create table user (name text, email text)')

    def run_job(self, d, now):
        run_luigi('exercises.complex.db_dump', 'UserDbSnapshot', host=self.db_host, date=d, now=now)

    @skip("Remove this line to enable the test")
    def test_happy(self):
        user_path = Path(f'{TEST_LAKE}/cold/UserSnapshot/year=2019/month=10/day=13')

        self.run_job('2019-10-13', now='2019-10-12T23:59:59')
        output = read_dataset(user_path)
        self.assertIsNone(output)

        self.db_execute('insert into user (name, email) values ("alice", "alice@gmail.com")')
        self.db_execute('insert into user (name, email) values ("bob", "bob@gmail.com")')

        # Try to run. This should not dump any data, since time is not yet past 01:00.
        self.run_job('2019-10-13', now='2019-10-13T00:59:59')
        output = read_dataset(user_path)
        self.assertIsNone(output)

        # We should now succeed
        self.run_job('2019-10-13', now='2019-10-13T01:01:00')
        output = read_dataset(user_path)
        self.assertCountEqual([{'name': 'alice', 'email': 'alice@gmail.com'},
                               {'name': 'bob', 'email': 'bob@gmail.com'}],
                              output)
