# Scling workflow orchestration exercises

Note: The workflow exercises are not finished. There is one working exercise, however, and you can
follow the setup instructions below in order to prepare for solving the exercises. Before starting
solving the exercises during the course, run `git pull` to get all of the workflow exercises.

## Getting started

### Learning a tiny bit of Python

The workflow exercises are written in Python, as are the recommended workflow orchestration tools
Luigi and Airflow. Hence, you will need to know some Python basics in order to do the exercises.

If you don't know Python, please read through [the official
tutorial](https://docs.python.org/3/tutorial/). You will not need to learn any advanced topics, only
the following topics:

* Basic syntax - basic control structures, variables, functions, classes.
* Basic data structures - lists and dictionaries.
* List comprehensions
* String formatting with interpolation syntax (f"foo={foo}").
* The datetime and Pathlib standard libraries.

### System prerequisites

* [Python, at least version 3.6](https://www.python.org/downloads/release/python-369/)
* [Git, a recent version](https://git-scm.com/downloads).
* [IntelliJ IDEA Community edition, a recent
  version](https://www.jetbrains.com/idea/download/#section=linux). Or PyCharm or another
  development environment of your choice.
* IntelliJ Python plugin. [Installed from within IntelliJ
  IDEA](https://www.jetbrains.com/help/idea/installing-updating-and-uninstalling-repository-plugins.html).

In the instructions below, `python3` is assumed to be at least Python 3.6, i.e. the version that you
downloaded, unless your system was already equipped with a sufficient version of Python.

Create a Python virtual environment, activate it, and install Luigi:

* `cd workflow`
* `python3 -m virtualenv --python=python3 venv`
* `source venv/bin/activate`
* `pip install -r requirements.txt`

If you wish, you do not need to create a virtual environment, and use the system Python installation
instead. In that case, install Luigi in your system Python environment:

* `sudo pip3 install -r requirements.txt`

Note pip3, and not pip. The latter manages Python 2 packages.

### Getting started

Each workflow exercise is an unfinished workflow DAG, which you are supposed to implement. The
workflow files are located under `workflow/exercises`. Each workflow file has a corresponding file
with test cases, and your task is to make the test cases pass. The test cases, located in
`workflow/test/*/test_*.py`, typically populate a test data lake with a few datasets, runs your
workflow, which in turn may run the simple programs provided in `workflow/bin` to perform data
processing.

When you decide to pick up a particular exercise, open the corresponding test case, and remove the
lines starting with `@skip`. Edit the workflow file matching the test case name until the tests pass.

### Running the tests

You have different options for running the tests. IntelliJ is most complex to set up, but provides
the best support for type assistance, quick iteration, and debugging. If you cannot get it to work,
you may opt for one of the other options

#### Developing and testing in IntelliJ

You will need to configure IntelliJ to use the appropriate Python interpreter. If you created a
virtual environment, you will need to inform IntelliJ with the steps below. Note that it might be
best to also have your virtual environment in PATH, i.e. by executing `source
workflow/venv/bin/activate` before starting IntelliJ. In order for this to have appropriate impact,
you will need to launch IntelliJ from the command line (`idea.sh` in Linux).

Configuring your virtual environment in IntelliJ:

* IntelliJ: File -> Project Structure -> SDKs -> Add New SDK (+ symbol) -> Python SDK -> Virtualenv
  environment -> Existing environment. 
* Select the file `workflow/venv/bin/python3`.

If you have set up IntelliJ for the batch processing exercises, there should be a module called
`workflow`, which should have the `Python` facet. Verify this in the project structure:

* IntelliJ: File -> Project Structure -> Facets.
* You should find `Python (workflow)` in the list. If it is absent, add it:
* IntelliJ: File -> Project Structure -> Facets -> Add (+ symbol) -> `workflow`.

Verify that we are using the right Python interpreter (the venv SDK you created above).

* IntelliJ: File -> Project Structure -> Modules -> `<repository_name>/workflow/workflow` -> Module
  SDK. Select the venv Python SDK.
* IntelliJ: File -> Project Structure -> Modules -> `<repository_name>/workflow/workflow/Python` ->
  Python interpreter. Select the venv Python SDK. 
  
Phew, lots of hoops with IntelliJ. In some cases, IntelliJ defaults to the right settings, but not
always. If you get issues with the Python setup, revisit these instructions. If you prefer PyCharm,
the instructions are more or less the same as for IntelliJ.

You should noe be able to run workflow tests inside IntelliJ. Open the file
`workflow/test/demo/test_demographic_sales.py`, but the cursor on the class `DemographicSalesTest`,
right click, and select `Run Unittests for test_demographic_sales.DemographicSalesTest`. If that
option does not show up, something is probably wrong with your IntelliJ setup, and it cannot deduce
that it is a unittest test case.

#### Python unittest framework, Gradle

In order to run the tests with Python unittest, go to the `workflow` directory and type:

* `PYTHONPATH=. python3 -m unittest discover`

Note that if you use a virtual environment, you have to have the virtual environment activated
(`source workflow/venv/bin/activate`).

In order to edit the exercise files, use any text editor you like, Emacs, Sublime, etc. Don't forget
to save before you run a test. :-)

You can run the Python tests via Gradle if you want:

* `cd .. && ./gradlew :workflow:pytest`

It is merely an alias for `python3 -m unittest discover`. The Gradle target `testAll` will run both
Scala and Python tests, verifying your full setup.


#### Inside Docker

In case you cannot get your environment to run the exercises, you can run the tests inside
Docker. You can still edit source files with your preferred editor, and changes will be picked up
inside the Docker container. First, remove any virtual environment you might have created, then
follow the instructions in [../README.md](../README.md). Summary below:

* `rm -rf venv`
* `cd ..`
* `./docker_gradle.sh`

You should now be inside a Docker container, and a virtual environment should be activated.

Windows users:
* `fromdos win_docker_test.sh`
* `./win_docker_test.sh :workflow:pytest`

Linux, MacOS users:
* `./gradlew :workflow:pytest`
