#!/usr/bin/env bash

set -e
#set -x

echo
echo "Building development environment image. This will be slow the first time."
echo
docker build . -t scling/big_data_exercises

echo
echo "Starting a shell inside the Docker container. In order to run tests, run 'fromdos win_docker_test.sh' once." 
echo "Then run './win_docker_test.sh' when you want to run the tests. Non-windows users can run './gradlew test' "
echo "instead."
echo
echo "The test script will be slow the first time, but then fast as long as you keep the container running."
echo "Type 'exit' when you want to leave the shell. No source files will be lost"
echo

docker run -it -v "$(pwd):/scling_exercise" scling/big_data_exercises bash
