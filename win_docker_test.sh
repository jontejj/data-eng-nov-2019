#! /usr/bin/env bash

set -e

mkdir -p /scling_exercise_unix
rsync -aP --delete --exclude out --exclude build --cvs-exclude ./* /scling_exercise_unix/
cd /scling_exercise_unix/
for f in $(find . -type f); do
  if file $f | grep -q 'with CRLF line'; then
    fromdos $f
  fi
done
./gradlew "${1:-test}"
