#! /usr/bin/env python3
import re
import subprocess
import tempfile
from pathlib import Path

import sys


def run(cmd, **kwargs):
    print(" ".join(cmd))
    subprocess.check_call(cmd, **kwargs)


def clean_job_source_scala(src_dir):
    for src in src_dir.glob("*/*.scala"):
        remove_solution_scala(src)


def clean_job_source_python(src_dir):
    for src in src_dir.glob("*/*.py"):
        remove_solution_python(src)


def remove_solution_scala(src_file: Path):
    print("Removing solution in {}".format(src_file))
    contents = src_file.read_text()
    test_file = Path(str(src_file).replace("/main/", "/test/").replace(".scala", "Test.scala"))
    test_contents = test_file.read_text()
    example_match = re.search(r'\n *test\("Example"\) *{\n(.*?)\n *assert\(', test_contents, flags=re.DOTALL)
    if example_match:
        example = example_match.group(1)
        example_text = ("\n\n// Example input and output (copied from {}):\n".format(str(test_file.name)) +
                        "/*\n" + example + "\n*/\n\n")
    else:
        example_text = ""
    without_solution = re.sub(
        r"// *solution_begin.*?// *solution_end",
        "\n\n    // Insert your solution here. Replace Coll.empty[...] below with something that you calculate:\n" +
        "/*\n" +
        "    def myMapFn(record: SomeType): SomeType = { ... }\n"
        "\n" +
        "    val tmp1 = input_dataset\n" +
        "      .map(myMapFn)\n" +
        '      .log("tmp1")\n' +
        '    val result = tmp1\n' +
        '      .filter(_.country == "SE")\n' +
        '      .log("result")\n'
        '*/\n\n',
        contents, flags=re.DOTALL)
    with_example = re.sub(r'\nobject ', example_text + '\nobject ', without_solution)
    new_contents = re.sub("// *exercise: *", "", with_example)
    src_file.write_text(new_contents)


def remove_solution_python(src_file: Path):
    print("Removing solution in {}".format(src_file))
    contents = src_file.read_text()
    without_solution = re.sub(
        r" *# *solution_begin.*? *# *solution_end",
        '    # Insert solution code here. Most Luigi tasks need one or more of:\n' +
        '    # job parameters (date, etc), requires() method, and output() or complete() method,\n' +
        '    # as well as methods specific to type of task, e.g. program_args (ExternalProgramTask), ' +
        'database (CopyToTable), ...\n\n',
        contents, flags=re.DOTALL)
    new_contents = re.sub("// *exercise: *", "", without_solution)
    src_file.write_text(new_contents)


def ignore_tests_scala(test_dir):
    for test_src in test_dir.glob("*/*.scala"):
        ignore_tests_in_scala(test_src)


def ignore_tests_in_scala(test_file: Path):
    print("Ignoring tests in {}".format(test_file))
    contents = test_file.read_text()
    ignored = contents.replace('test("', 'ignore("')
    new_contents = ignored.replace('ignore("Empty input")', 'test("Empty input")')
    test_file.write_text(new_contents)


def ignore_tests_python(test_dir):
    for test_src in test_dir.glob("*/*.py"):
        ignore_tests_in_python(test_src)


def ignore_tests_in_python(test_file: Path):
    print("Ignoring tests in {}".format(test_file))
    contents = test_file.read_text()
    ignored = contents.replace('# @skip', '@skip')
    new_contents = ignored.replace('ignore("Empty input")', 'test("Empty input")')
    test_file.write_text(new_contents)


def patch_all_files(repo: Path):
    clean_job_source_scala(repo / "src/main/scala/com/scling/exercises")
    ignore_tests_scala(repo / "src/test/scala/com/scling/exercises")
    clean_job_source_python(repo / 'workflow/exercises')
    ignore_tests_python(repo / 'workflow/test')
    run(["./gradlew", "testAll"], cwd=str(repo))


def clone(repo_name):
    tmp_dir = tempfile.mkdtemp(prefix="scling_exercise_")
    print("Cloning {} in {}".format(repo_name, tmp_dir))
    run(["git", "clone", "git@gitlab.com:scling/tiny-big-data.git", repo_name], cwd=tmp_dir)
    repo = Path(tmp_dir) / repo_name
    return repo


def push_solutions(repo_name: str, repo: Path):
    run(["git", "push", "--set-upstream", f"git@gitlab.com:scling-public/{repo_name}.git", "master"], cwd=str(repo))
    run(["git", "remote", "add", "course", f"git@gitlab.com:scling-public/{repo_name}.git"], cwd=str(repo))
    run(["git", "checkout", "-b", "solutions"], cwd=str(repo))
    run(["git", "push", "--set-upstream", "course", "solutions"], cwd=str(repo))
    run(["git", "checkout", "master"], cwd=str(repo))


def push_master(repo):
    run(["git", "add", "."], cwd=str(repo))
    run(["git", "commit", "-m", "Remove solutions, ignore tests."], cwd=str(repo))
    run(["git", "push", "course"], cwd=str(repo))


def main(argv):
    if argv[1] == '-l':
        patch_all_files(Path('.'))
    elif argv[1] == "-n":
        repo_name = argv[2]
        repo = clone(repo_name)
        patch_all_files(repo)
    elif argv[1] == '-x':
        repo_name = argv[2]
        repo = clone(repo_name)
        push_solutions(repo_name, repo)
        patch_all_files(repo)
        push_master(repo)
    else:
        raise RuntimeError("Use -l -n or -x")

if __name__ == '__main__':
    main(sys.argv)
