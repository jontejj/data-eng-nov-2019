package com.scling.exercises.simple

import com.github.nscala_time.time.Imports._

import better.files.File
import com.scling.exercises._
import org.joda.time.DateTime
import org.joda.time.DateTimeZone.UTC
import org.scalatest.FunSuite

class CountryCountJobTest extends FunSuite with BatchJobTestRunner2[Order, User, CountryCount] {

  import Serde._

  implicit val countryCountOrdering: Ordering[CountryCount] = Ordering.by(CountryCount.unapply)

  override def runJob(orderFile: File, userFile: File, outputFile: File): Unit =
    CountryCountJob.main(Array("CountryCountJob", orderFile.toString, userFile.toString, outputFile.toString))

  test("Empty input") {
    assert(runTest(Seq(), Seq()) === Seq())
  }

  val millenial = new DateTime(2000, 1, 1, 0, 0, UTC)

  ignore("Single order") {
    assert(runTest(
      Seq(Order("1", "alice", "pants")),
      Seq(User("alice", "Alice Springs", "SE", millenial, "female"))) ===
      Seq(CountryCount("SE", 1)))
  }

  ignore("Two users, same country") {
    assert(runTest(
      Seq(
        Order("1", "alice", "pants"),
        Order("2", "cecilia", "pants")),
      Seq(
        User("alice", "Alice Springs", "SE", millenial, "female"),
        User("cecilia", "Cecilia Smith", "SE", millenial, "female")
      )) ===
      Seq(CountryCount("SE", 2)))
  }

  ignore("Two countries") {
    assert(runTest(
      Seq(
        Order("1", "alice", "pants"),
        Order("2", "cecilia", "pants"),
        Order("3", "bob", "shirt"),
        Order("4", "david", "shirt"),
        Order("5", "bob", "shirt")),
      Seq(
        User("alice", "Alice Springs", "SE", millenial, "female"),
        User("cecilia", "Cecilia Smith", "SE", millenial, "female"),
        User("bob", "Bob Hope", "UK", millenial + 1.year, "male"),
        User("david", "David Beckham", "UK", millenial + 1.year, "male")
      )) ===
      Seq(
        CountryCount("SE", 2),
        CountryCount("UK", 3)
      ))
  }

  ignore("No matching user") {
    assert(runTest(
      Seq(
        Order("1", "alice", "pants"),
        Order("2", "cecilia", "pants")),
      Seq(
        User("alice", "Alice Springs", "SE", millenial, "female")
      )) ===
      Seq(CountryCount("SE", 1)))
  }
}
