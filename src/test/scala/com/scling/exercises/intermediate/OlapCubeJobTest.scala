package com.scling.exercises.intermediate

import better.files.File
import com.github.nscala_time.time.Imports._
import com.scling.exercises._
import org.joda.time.DateTime
import org.joda.time.DateTimeZone.UTC
import org.scalatest.FunSuite

class OlapCubeJobTest extends FunSuite with BatchJobTestRunner2[Order, User, OrderAggregate] {

  import Serde._

  implicit val orderAggregateOrdering: Ordering[OrderAggregate] = Ordering.by(OrderAggregate.unapply)

  override def runJob(orderFile: File, userFile: File, outputFile: File): Unit =
    OlapCubeJob.main(Array("OlapCubeJob", "2018-03-22", orderFile.toString, userFile.toString, outputFile.toString))

  test("Empty input") {
    assert(runTest(Seq(), Seq()) === Seq())
  }

  val millenial = new DateTime(2000, 1, 1, 0, 0, UTC)

  ignore("Single order") {
    assert(runTest(
      Seq(Order("1", "alice", "pants")),
      Seq(User("alice", "Alice Springs", "SE", millenial, "female"))) ===
      Seq(OrderAggregate("pants", "SE", 18, "female", 1)))
  }

  ignore("Two users, same group") {
    assert(runTest(
      Seq(
        Order("1", "alice", "pants"),
        Order("2", "cecilia", "pants")),
      Seq(
        User("alice", "Alice Springs", "SE", millenial, "female"),
        User("cecilia", "Cecilia Smith", "SE", millenial, "female")
      )) ===
      Seq(OrderAggregate("pants", "SE", 18, "female", 2)))
  }

  ignore("Two groups") {
    assert(runTest(
      Seq(
        Order("1", "alice", "pants"),
        Order("2", "cecilia", "pants"),
        Order("3", "bob", "shirt"),
        Order("4", "david", "shirt"),
        Order("5", "bob", "shirt")),
      Seq(
        User("alice", "Alice Springs", "SE", millenial, "female"),
        User("cecilia", "Cecilia Smith", "SE", millenial, "female"),
        User("bob", "Bob Hope", "SE", millenial + 1.year, "male"),
        User("david", "David Beckham", "SE", millenial + 1.year, "male")
      )) ===
      Seq(
        OrderAggregate("pants", "SE", 18, "female", 2),
        OrderAggregate("shirt", "SE", 17, "male", 3)
      ))
  }

  ignore("No matching user") {
    assert(runTest(
      Seq(
        Order("1", "alice", "pants"),
        Order("2", "cecilia", "pants")),
      Seq(
        User("alice", "Alice Springs", "SE", millenial, "female")
      )) ===
      Seq(OrderAggregate("pants", "SE", 18, "female", 1)))
  }
}
