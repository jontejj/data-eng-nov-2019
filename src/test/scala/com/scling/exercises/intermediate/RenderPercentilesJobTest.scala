package com.scling.exercises.intermediate

import better.files.File
import com.scling.exercises._
import org.scalatest.FunSuite

class RenderPercentilesJobTest extends FunSuite with BatchJobTestRunner1[PageRender, RenderPercentiles] {
  import Serde._
  import TestInput._

  implicit val renderPercentilesOrdering: Ordering[RenderPercentiles] = Ordering.by(RenderPercentiles.unapply)

  override def runJob(pageRender: File, output: File): Unit =
    RenderPercentilesJob.main(Array("PageRenderJob", pageRender.toString, output.toString))

  test("Empty input") {
    assert(runTest(Seq()) === Seq())
  }

    val root = "http://www.ex.com/"

  ignore("One item") {
    assert(runTest(Seq(PageRender(root, 20))) === Seq(RenderPercentiles(root, 20, 20, 20, 20, 20, 20, 20)))
  }

  ignore("1 percent") {
    assert(runTest(Seq(PageRender(root, 20)) ++ Seq.fill(98)(PageRender(root, 30))) ===
      Seq(RenderPercentiles(root, 20, 30, 30, 30, 30, 30, 30)))
  }

  ignore("5 percent") {
    assert(runTest(Seq.fill(5)(PageRender(root, 20)) ++ Seq.fill(94)(PageRender(root, 30))) ===
      Seq(RenderPercentiles(root, 20, 20, 30, 30, 30, 30, 30)))
  }

  ignore("10 percent") {
    assert(runTest(Seq.fill(10)(PageRender(root, 20)) ++ Seq.fill(89)(PageRender(root, 30))) ===
      Seq(RenderPercentiles(root, 20, 20, 20, 30, 30, 30, 30)))
  }

  ignore("50 percent") {
    assert(runTest(Seq.fill(50)(PageRender(root, 20)) ++ Seq.fill(49)(PageRender(root, 30))) ===
      Seq(RenderPercentiles(root, 20, 20, 20, 20, 30, 30, 30)))
  }

  ignore("90 percent") {
    assert(runTest(Seq.fill(90)(PageRender(root, 20)) ++ Seq.fill(9)(PageRender(root, 30))) ===
      Seq(RenderPercentiles(root, 20, 20, 20, 20, 20, 30, 30)))
  }

  ignore("95 percent") {
    assert(runTest(Seq.fill(4)(PageRender(root, 30)) ++ Seq.fill(95)(PageRender(root, 20))) ===
      Seq(RenderPercentiles(root, 20, 20, 20, 20, 20, 20, 30)))
  }

  ignore("99 percent") {
    assert(runTest(Seq.fill(100)(PageRender(root, 20)) ++ Seq.fill(1)(PageRender(root, 30))) ===
      Seq(RenderPercentiles(root, 20, 20, 20, 20, 20, 20, 20)))
  }

  ignore("Two URLs") {
    val page1 = root + "page1"
    assert(runTest(
      Seq.fill(90)(PageRender(root, 20)) ++ Seq.fill(9)(PageRender(root, 30)) ++
      Seq.fill(10)(PageRender(page1, 20)) ++ Seq.fill(89)(PageRender(page1, 30))
    ) ===
      Seq(RenderPercentiles(root, 20, 20, 20, 20, 20, 30, 30),
        RenderPercentiles(page1, 20, 20, 20, 30, 30, 30, 30)))
  }

}
