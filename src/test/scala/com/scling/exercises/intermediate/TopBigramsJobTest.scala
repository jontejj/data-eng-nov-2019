package com.scling.exercises.intermediate

import better.files.File
import com.scling.exercises.{BatchJobTestRunner1, BigramCount, MessagePost, Serde}
import org.scalatest.FunSuite

class TopBigramsJobTest extends FunSuite with BatchJobTestRunner1[MessagePost, BigramCount] {
  import Serde._

  implicit val bigramCountOrdering: Ordering[BigramCount] = Ordering.by(BigramCount.unapply)

  def runJob(input: File, output: File): Unit =
    TopBigramsJob.main(Array("TopBigramsJob", "3", input.toString, output.toString))

  test("Empty input") {
    assert(runTest(Seq()) === Seq())
  }

  ignore("Single word") {
    val input = Seq(MessagePost("one"))
    assert(runTest(input) === Seq())
  }

  ignore("One bigram") {
    val input = Seq(MessagePost("one two"))
    assert(runTest(input) === Seq(BigramCount("one", "two", 1)))
  }

  ignore("Odd characters") {
    val input = Seq(MessagePost("one.two"))
    assert(runTest(input) === Seq(BigramCount("one", "two", 1)))
  }

  ignore("Multiple separators") {
    val input = Seq(MessagePost("one  two"))
    assert(runTest(input) === Seq(BigramCount("one", "two", 1)))
  }

  ignore("Two bigrams") {
    val input = Seq(MessagePost("one two three"))
    assert(runTest(input) === Seq(BigramCount("one", "two", 1), BigramCount("two", "three", 1)))
  }

  ignore("Multiple posts") {
    val input = Seq(
      MessagePost("one two three"),
      MessagePost("two three four"))

    assert(runTest(input) === Seq(
      BigramCount("one", "two", 1),
      BigramCount("three", "four", 1),
      BigramCount("two", "three", 2)
    ))
  }

  ignore("Drop least common") {
    val input = Seq(
      MessagePost("one two three one two three"),
      MessagePost("two three four"),
      MessagePost("two three four five"))

    assert(runTest(input) === Seq(
      BigramCount("one", "two", 2),
      BigramCount("three", "four", 2),
      BigramCount("two", "three", 4)
    ))
  }
}
