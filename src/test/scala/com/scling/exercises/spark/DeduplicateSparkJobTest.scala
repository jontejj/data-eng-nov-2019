package com.scling.exercises.spark

import better.files.File
import com.scling.exercises.{BatchJobTestRunner1, Order, Serde}
import org.apache.spark.sql.SparkSession
import org.scalatest.FunSuite


class DeduplicateSparkJobTest extends FunSuite with BatchJobTestRunner1[Order, Order]  {

  import Serde._

  implicit val orderOrdering: Ordering[Order] = Ordering.by(Order.unapply)

  // We cache the spark session for speed
  private val spark = SparkSession
    .builder()
    // The default 200 shufflers make tests slower.
    .config("spark.sql.shuffle.partitions", "1")
    .master("local[1]")
    .getOrCreate()

  override def runJob(inputPath: File, outputPath: File): Unit =
    DeduplicateSparkJob.run(Array("DeduplicateSparkJob", inputPath.toString, outputPath.toString), spark)

  // Spark does not seem to like empty inputs, ignore this one.
  //  test("Empty input") {
  //    assert(runTest(Seq()) === Seq())
  //  }

  ignore("Single item") {
    val input = Seq(Order("1", "alice", "pants"))
    assert(runTest(input) === input)
  }

  ignore("Distinct items") {
    val input = Seq(Order("1", "alice", "pants"), Order("2", "bob", "shirt"))
    assert(runTest(input) === input)
  }

  ignore("One duplicate") {
    val input = Seq(Order("1", "alice", "pants"), Order("1", "alice", "pants"), Order("2", "bob", "shirt"))
    assert(runTest(input) === input.tail)
  }

  ignore("Duplicate, different fields") {
    val input = Seq(Order("1", "alice", "pants"), Order("1", "Alice again", "pants"), Order("2", "bob", "shirt"))
    assert(runTest(input).map(_.id) === Seq("1", "2"))
  }

  ignore("Two duplicates") {
    val input = Seq(
      Order("1", "alice", "pants"),
      Order("1", "alice", "pants"),
      Order("2", "bob", "shirt"),
      Order("3", "cecilia", "skirt"),
      Order("2", "bob", "shirt"))
    assert(runTest(input) === input.tail.dropRight(1))
  }

  ignore("Double duplicate") {
    val input = Seq(
      Order("1", "alice", "pants"),
      Order("2", "bob", "shirt"),
      Order("3", "cecilia", "skirt"),
      Order("2", "bob", "shirt"),
      Order("2", "bob", "shirt"))
    assert(runTest(input) === input.dropRight(2))
  }
}
