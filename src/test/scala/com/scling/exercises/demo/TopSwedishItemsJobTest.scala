package com.scling.exercises.demo

import better.files.File
import com.github.nscala_time.time.Imports._
import com.scling.exercises._
import org.joda.time.DateTime
import org.joda.time.DateTimeZone.UTC
import org.scalatest.FunSuite

class TopSwedishItemsJobTest extends FunSuite with BatchJobTestRunner2[Order, User, ItemCount] {

  import Serde._
  import TestInput._

  implicit val ItemCountOrdering: Ordering[ItemCount] = Ordering.by(ItemCount.unapply)

  override def runJob(orderFile: File, userFile: File, outputFile: File): Unit =
    TopSwedishItemsJob.main(Array("TopSwedishItemsJob", "3", orderFile.toString, userFile.toString, outputFile.toString))

  test("Empty input") {
    assert(runTest(Seq(), Seq()) === Seq())
  }

  val millenial = new DateTime(2000, 1, 1, 0, 0, UTC)

  ignore("Single Swedish order") {
    assert(runTest(
      Seq(Order("1", "alice", "pants")),
      Seq(User("alice", "Alice Springs", "SE", millenial, "female"))) ===
      Seq(ItemCount("pants", 1)))
  }

  ignore("Capitalised Swedish name") {
    assert(runTest(
      Seq(Order("1", "Alice", "pants")),
      Seq(User("alice", "Alice Springs", "SE", millenial, "female"))) ===
      Seq(ItemCount("pants", 1)))
  }

  ignore("Two users, both Swedish") {
    assert(runTest(
      Seq(
        Order("1", "alice", "pants"),
        Order("2", "cecilia", "pants")),
      Seq(
        User("alice", "Alice Springs", "SE", millenial, "female"),
        User("cecilia", "Cecilia Smith", "SE", millenial, "female")
      )) ===
      Seq(ItemCount("pants", 2)))
  }

  ignore("Two countries, two items") {
    assert(runTest(
      Seq(
        Order("1", "alice", "pants"),
        Order("2", "cecilia", "pants"),
        Order("3", "alice", "shirt"),
        Order("4", "david", "shirt"),
        Order("5", "bob", "shirt")),
      Seq(
        User("alice", "Alice Springs", "SE", millenial, "female"),
        User("cecilia", "Cecilia Smith", "SE", millenial, "female"),
        User("bob", "Bob Hope", "UK", millenial + 1.year, "male"),
        User("david", "David Beckham", "UK", millenial + 1.year, "male")
      )) ===
      Seq(
        ItemCount("pants", 2),
        ItemCount("shirt", 1)
      ))
  }

  ignore("No matching user") {
    assert(runTest(
      Seq(
        Order("1", "alice", "pants"),
        Order("2", "cecilia", "pants")),
      Seq(
        User("alice", "Alice Springs", "SE", millenial, "female")
      )) ===
      Seq(ItemCount("pants", 1)))
  }

  ignore("Only top items") {
    val input =
      orders(6, "bob", "beanie") ++
        orders(7, "cecilia", "beanie") ++
        orders(6, "Alice", "pants") ++
        orders(2, "Cecilia", "skirt") ++
        orders(1, "cecilia", "trousers") ++
        orders(1, "david", "shirt")
    val users = Seq(
      User("alice", "Alice Springs", "SE", millenial, "female"),
      User("cecilia", "Cecilia Smith", "SE", millenial, "female"),
      User("bob", "Bob Hope", "UK", millenial + 1.year, "male"),
      User("david", "David Beckham", "UK", millenial + 1.year, "male")
    )

    assert(runTest(input, users) === Seq(
      ItemCount("beanie", 7),
      ItemCount("pants", 6),
      ItemCount("skirt", 2)
    ))
  }
}
