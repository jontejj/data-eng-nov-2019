package com.scling.tinybigdata

import org.scalatest.FunSuite

class KCollTest extends FunSuite {

  test("from empty") {
    assert(KColl.from[Int, String](Map()).collect() === List.empty[(Int, String)])
  }

  test("from") {
    assert(KColl.from[Int, String](Map(1 -> List("a"), 2 -> List("b"))).collect() ===
      List((1, "a"), (2, "b")))
  }

  test("from multiple") {
    assert(KColl.from[Int, String](Map(1 -> List("a"), 2 -> List("b", "c"))).collect() ===
      List((1, "a"), (2, "b"), (2, "c")))
  }

  test("values") {
    assert(KColl.from(Map(1 -> List("a"))).values.collect() === List("a"))
  }

  test("values multi") {
    assert(KColl.from(Map(1 -> List("a"), 2 -> List("b"))).values.collect() === List("a", "b"))
  }

  test("fold") {
    assert(KColl.from(Map(1 -> List("a", "b"))).fold("c")((c, str) => c + str).collect() === List((1, "cab")))
  }

  test("fold sequence") {
    val input = KColl.from(Map(1 -> List("a", "b")))
    val output = input.fold(Seq[String]())(_ :+ _)
    assert(output.collect() === List((1, Seq("a", "b"))))
  }

  test("foldWithKey") {
    val input = KColl.from(Map(1 -> List("a", "b")))
    val output = input.foldWithKey(key => Seq[String](key.toString))(_ :+ _)
    assert(output.collect() === List((1, Seq("1", "a", "b"))))
  }

  test("reduce") {
    assert(KColl.from(Map(1 -> List("a", "b", "c"))).reduce(_ + _).collect() === List((1, "abc")))
  }

  test("sortBy") {
    assert(KColl.from(Map(1 -> List("a", "e", "c", "b"))).sortBy(identity).values.collect() ===
      List("a", "b", "c", "e"))
  }

  test("join matching") {
    val left = KColl.from(Map(1 -> List("a")))
    val right = KColl.from(Map(1 -> List("b")))
    assert(left.join(right).collect() === List((1, ("a", "b"))))
  }

  test("join no match") {
    val left = KColl.from(Map(1 -> List("a")))
    val right = KColl.from(Map(2 -> List("b")))
    assert(left.join(right).collect() === List())
  }

  test("join one right match") {
    val left = KColl.from(Map(1 -> List("a")))
    val right = KColl.from(Map(1 -> List("b"), 2 -> List("c")))
    assert(left.join(right).collect() === List((1, ("a", "b"))))
  }

  test("join one left match") {
    val left = KColl.from(Map(1 -> List("a"), 2 -> List("c")))
    val right = KColl.from(Map(1 -> List("b")))
    assert(left.join(right).collect() === List((1, ("a", "b"))))
  }

  test("join multiple left matches") {
    val left = KColl.from(Map(1 -> List("a", "d")))
    val right = KColl.from(Map(1 -> List("b")))
    assert(left.join(right).collect() === List((1, ("a", "b")), (1, ("d", "b"))))
  }

  test("join multiple right matches") {
    val left = KColl.from(Map(1 -> List("a")))
    val right = KColl.from(Map(1 -> List("b", "c")))
    assert(left.join(right).collect() === List((1, ("a", "b")), (1, ("a", "c"))))
  }

  test("join multiple left and right matches") {
    val left = KColl.from(Map(1 -> List("a", "d")))
    val right = KColl.from(Map(1 -> List("b", "c")))
    assert(left.join(right).collect() ===
      List((1, ("a", "b")),
        (1, ("a", "c")),
        (1, ("d", "b")),
        (1, ("d", "c"))))
  }

  test("leftJoin matching") {
    val left = KColl.from(Map(1 -> List("a")))
    val right = KColl.from(Map(1 -> List("b")))
    assert(left.leftJoin(right).collect() === List((1, ("a", Some("b")))))
  }

  test("leftJoin no match") {
    val left = KColl.from(Map(1 -> List("a")))
    val right = KColl.from(Map(2 -> List("b")))
    assert(left.leftJoin(right).collect() === List((1, ("a", None))))
  }

  test("leftJoin one right match") {
    val left = KColl.from(Map(1 -> List("a")))
    val right = KColl.from(Map(1 -> List("b"), 2 -> List("c")))
    assert(left.leftJoin(right).collect() === List((1, ("a", Some("b")))))
  }

  test("leftJoin one left match") {
    val left = KColl.from(Map(1 -> List("a"), 2 -> List("c")))
    val right = KColl.from(Map(1 -> List("b")))
    assert(left.leftJoin(right).collect() === List((1, ("a", Some("b"))), (2, ("c", None))))
  }

  test("leftJoin multiple left matches") {
    val left = KColl.from(Map(1 -> List("a", "d")))
    val right = KColl.from(Map(1 -> List("b")))
    assert(left.leftJoin(right).collect() === List((1, ("a", Some("b"))), (1, ("d", Some("b")))))
  }

  test("leftJoin multiple right matches") {
    val left = KColl.from(Map(1 -> List("a")))
    val right = KColl.from(Map(1 -> List("b", "c")))
    assert(left.leftJoin(right).collect() === List((1, ("a", Some("b"))), (1, ("a", Some("c")))))
  }

  test("leftJoin multiple left and right matches") {
    val left = KColl.from(Map(1 -> List("a", "d")))
    val right = KColl.from(Map(1 -> List("b", "c")))
    assert(left.leftJoin(right).collect() ===
      List((1, ("a", Some("b"))),
        (1, ("a", Some("c"))),
        (1, ("d", Some("b"))),
        (1, ("d", Some("c")))))
  }

  test("count") {
    assert(KColl.from(Map(1 -> List("a"))).count.collect() === List((1, 1)))
  }

  test("take") {
    assert(KColl.from(Map(1 -> List("a", "e", "c", "b"))).take(2).collect() ===
      List((1, "a"), (1, "e")))
  }

  test("map") {
    assert(KColl.from(Map(1 -> List(3, 5))).map(_._2 + 3).collect().sorted === List(6, 8))
  }

  test("union") {
    assert(KColl.from(Map(1 -> List(2))).union(KColl.from(Map(3 -> List(4, 5)))).collect().sorted ===
      List((1, 2), (3, 4), (3, 5)))
  }

  test("toString") {
    assert(KColl.from(Map(1 -> List("a"))).toString === "{\n  1 -> [\n    a\n  ]}")
  }
}
