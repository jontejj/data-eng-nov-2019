package com.scling.exercises.demo

import com.scling.exercises.Serde._
import com.scling.exercises._
import com.scling.tinybigdata.{Coll, KColl}

// Read a dataset with orders. Join with the user dataset, which contains country of residence and emit the n most
// popular items in Sweden. Some usernames have been capitalised by accident, and must be converted to lower case.

object TopSwedishItemsJob {
  def main(args: Array[String]): Unit = {
    val topCount = args(1).toInt
    val orderPath = args(2)
    val userPath = args(3)
    val outputPath = args(4)

    val order = Coll.readJson[Order](orderPath)
    val user = Coll.readJson[User](userPath)

    

    // Insert your solution here. Replace Coll.empty[...] below with something that you calculate:
/*
    def myMapFn(record: SomeType): SomeType = { ... }

    val tmp1 = input_dataset
      .map(myMapFn)
      .log("tmp1")
    val result = tmp1
      .filter(_.country == "SE")
      .log("result")
*/


    val result: Coll[ItemCount] = Coll.empty[ItemCount]
    result.writeJson(outputPath)
  }
}
