package com.scling.exercises

import com.github.nscala_time.time.Imports.DateTime

// Schema definitions and related utilities

object Schema {}

case class BigramCount(first: String, second: String, count: Long)


case class CountryCount(country: String, count: Long)

case class ItemDelta(item: String, previousCount: Long, newCount: Long) {
  def increase: Float = (newCount - previousCount) / previousCount.toFloat
}

case class MessagePost(content: String)

case class Order(id: String, user: String, item: String)
case class OrderAggregate(item: String, country: String, age: Int, gender: String, count: Long)

case class PageRender(url: String, renderTimeMillis: Int)

case class PageView(user: String, url: String, time: DateTime)

case class Recommendation(user: String, url: String)

case class RenderPercentiles(url: String,
                             percentile1Millis: Int,
                             percentile5Millis: Int,
                             percentile10Millis: Int,
                             percentile50Millis: Int,
                             percentile90Millis: Int,
                             percentile95Millis: Int,
                             percentile99Millis: Int)

case class Session(user: String, start: DateTime, end: DateTime)

case class ItemCount(item: String, count: Long)
case class TrendingItem(item: String, position: Int, increasePercent: Int)

case class User(id: String, name: String, country: String, birthDate: DateTime, gender: String)
case class UserLastActive(id: String, name: String, lastActive: DateTime)

case class ViewTimePercent(seconds: Int, percent: Int)
case class ViewTimeDecline(url: String, viewCount: Long, viewTimePercentages: List[ViewTimePercent])
