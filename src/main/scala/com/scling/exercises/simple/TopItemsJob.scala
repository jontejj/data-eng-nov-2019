package com.scling.exercises.simple

import com.scling.exercises.{ItemCount, Order, Serde}
import com.scling.tinybigdata.Coll

// Given a dataset of orders, emit a dataset with the n most popular items, with counts.


object TopItemsJob {
  def main(args: Array[String]): Unit = {
    import Serde._

    val topCount = args(1).toInt
    val inputFile = args(2)
    val outputFile = args(3)

    val orders = Coll.readJson[Order](inputFile)

    

    // Insert your solution here. Replace Coll.empty[...] below with something that you calculate:
/*
    def myMapFn(record: SomeType): SomeType = { ... }

    val tmp1 = input_dataset
      .map(myMapFn)
      .log("tmp1")
    val result = tmp1
      .filter(_.country == "SE")
      .log("result")
*/


    val result: Coll[ItemCount] = Coll.empty[ItemCount]

    result.writeJson(outputFile)
  }
}
