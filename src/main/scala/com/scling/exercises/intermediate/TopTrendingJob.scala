package com.scling.exercises.intermediate

import com.scling.exercises.{ItemDelta, Order, Serde, TrendingItem}
import com.scling.tinybigdata.{Coll, KColl}


// Given a dataset with today's orders and one with yesterday's orders, emit a top list with the n items that have the
// highest relative increase in volume. The top list output is unordered, since it is a dataset, but each record should
// have a field denoting the position in the top list. The increase, rounded to whole percents, should also be included.

object TopTrendingJob {

  def main(args: Array[String]): Unit = {
    import Serde._

    val topCount = args(1).toInt
    val yesterdayPath = args(2)
    val todayPath = args(3)
    val outputPath = args(4)

    val yesterday = Coll.readJson[Order](yesterdayPath)
    val today = Coll.readJson[Order](todayPath)

    

    // Insert your solution here. Replace Coll.empty[...] below with something that you calculate:
/*
    def myMapFn(record: SomeType): SomeType = { ... }

    val tmp1 = input_dataset
      .map(myMapFn)
      .log("tmp1")
    val result = tmp1
      .filter(_.country == "SE")
      .log("result")
*/


    val result: Coll[TrendingItem] = Coll.empty[TrendingItem]
    result.writeJson(outputPath)
  }
}
