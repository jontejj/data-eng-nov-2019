package com.scling.exercises.intermediate

import com.github.nscala_time.time.Imports._
import com.scling.exercises.Serde._
import com.scling.exercises.{PageView, Session}
import com.scling.tinybigdata.Coll

// A web session is a sequence of page views from one user at most five minutes apart. A session can be no longer than
// three hours. Given a dataset of page views, emit a dataset with sessions.

object UserSessionsJob {
  def main(args: Array[String]): Unit = {
    val pageViewPath = args(1)
    val outputPath = args(2)

    val pageViews = Coll.readJson[PageView](pageViewPath)

    

    // Insert your solution here. Replace Coll.empty[...] below with something that you calculate:
/*
    def myMapFn(record: SomeType): SomeType = { ... }

    val tmp1 = input_dataset
      .map(myMapFn)
      .log("tmp1")
    val result = tmp1
      .filter(_.country == "SE")
      .log("result")
*/


    val result: Coll[Session] = Coll.empty[Session]
    result.writeJson(outputPath)
  }

}
