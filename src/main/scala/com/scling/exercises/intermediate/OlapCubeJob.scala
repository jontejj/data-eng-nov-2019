package com.scling.exercises.intermediate

import com.github.nscala_time.time.Imports._
import com.scling.exercises.Serde._
import com.scling.exercises.{Order, OrderAggregate, User}
import com.scling.tinybigdata.Coll
import org.joda.time.PeriodType
import org.joda.time.format.ISODateTimeFormat

// Read a dataset with orders. Join with users and aggregate counts on these dimensions: country, age, gender, item.

object OlapCubeJob {
  def main(args: Array[String]): Unit = {
    val date = ISODateTimeFormat.date().parseDateTime(args(1))
    val orderPath = args(2)
    val userPath = args(3)
    val outputPath = args(4)

    val order = Coll.readJson[Order](orderPath)
    val user = Coll.readJson[User](userPath)

    def calculateAge(birthDate: DateTime): Int = (birthDate to date).toPeriod(PeriodType.years()).years

    

    // Insert your solution here. Replace Coll.empty[...] below with something that you calculate:
/*
    def myMapFn(record: SomeType): SomeType = { ... }

    val tmp1 = input_dataset
      .map(myMapFn)
      .log("tmp1")
    val result = tmp1
      .filter(_.country == "SE")
      .log("result")
*/


    val result: Coll[OrderAggregate] = Coll.empty[OrderAggregate]

    result.writeJson(outputPath)
  }
}

// Internal useful utility class. An order decorated with demographic information.
case class OrderDemographical(id: String, item: String, country: String, age: Int, gender: String)

