package com.scling.exercises.complex

import com.github.nscala_time.time.Imports._
import com.scling.exercises._
import com.scling.tinybigdata.Coll
import org.joda.time.Duration


// Read a dataset with page view events, containing web page URLs, user, and time stamps. Determine the view time
// for each page view by comparing with the next view by the same user, capped to three minutes. For each url,
// emit the fraction of users still viewing a page after n seconds, with n ranging from 10 seconds to 170 seconds by
// 10 second increases..

object ViewTimeDeclineJob {

  

    // Insert your solution here. Replace Coll.empty[...] below with something that you calculate:
/*
    def myMapFn(record: SomeType): SomeType = { ... }

    val tmp1 = input_dataset
      .map(myMapFn)
      .log("tmp1")
    val result = tmp1
      .filter(_.country == "SE")
      .log("result")
*/



  def main(args: Array[String]): Unit = {
    import Serde._

    val pageViewPath = args(1)
    val outputPath = args(2)

    val pageView = Coll.readJson[PageView](pageViewPath)

    

    // Insert your solution here. Replace Coll.empty[...] below with something that you calculate:
/*
    def myMapFn(record: SomeType): SomeType = { ... }

    val tmp1 = input_dataset
      .map(myMapFn)
      .log("tmp1")
    val result = tmp1
      .filter(_.country == "SE")
      .log("result")
*/


    val result: Coll[ViewTimeDecline] = Coll.empty[ViewTimeDecline]
    result.writeJson(outputPath)
  }
}
